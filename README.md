# Isso

Isso is a self hosted comment system.

This repository/image is using the official source for Isso building for amd64 and
aarch64 via gitlab-ci.

For more information about Isso please check out the [github repo](https://github.com/posativ/isso) or official [webpage](https://posativ.org/isso/).  

Isso is released under the [MIT  license](https://github.com/posativ/isso/blob/master/LICENSE).


---

Currently, only the master branch is built. When the 0.12.3 version is released this will also
be included in the build script while the master branch will keep on building on a schedule.
